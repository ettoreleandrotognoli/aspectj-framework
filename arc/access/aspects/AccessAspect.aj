package arc.access.aspects;

import arc.access.AbstractAccessChecker;
import arc.access.AccessChecker;
import arc.access.AccessType;
import arc.access.annotations.AccessPermission;
import arc.access.exception.AccessException;
import arc.access.exception.AccessExceptionHandler;

public aspect AccessAspect {
	
	private static AccessChecker checker = new AbstractAccessChecker(){};
	private static AccessExceptionHandler handler = new AccessExceptionHandler(){
		@Override
		public void catchAccessException(AccessException exception) {
			exception.printStackTrace();
		}
	};
	
	public static void setAccessChecker(AccessChecker checker){
		if(checker!=null){
			AccessAspect.checker = checker;	
		}
	}
	
	public static void setAccessExceptionHandler(AccessExceptionHandler handler){
		if(handler != null){
			AccessAspect.handler = handler;	
		}
	}
	
	public static boolean check(AccessPermission accessPermission,AccessType accessType){
		if(accessPermission == null)
			return true;
		return AccessAspect.checker.check(accessPermission,accessType);
	}
	
	pointcut methodAnnotated(Object source,AccessPermission access) :
		execution(@AccessPermission * *(**))
		&& @annotation(access)
		&& target(source);
	
	pointcut classAnnotated(Object source,AccessPermission access):
		execution(!@AccessPermission * *.*(..))
		&& within(@AccessPermission *)
		&& target(source)
		&& @target(access);
	
	Object around(Object source,AccessPermission access):(classAnnotated(source,access)){
		if(!AccessAspect.checker.check(access,AccessType.EXECUTE))
			AccessAspect.handler.catchAccessException(new AccessException("Ainda não Implementado"));
		else
			return proceed(source,access);
		return null;
	}
	
	Object around(Object source,AccessPermission access):(methodAnnotated(source,access)){
		if(!AccessAspect.checker.check(access,AccessType.EXECUTE))
			AccessAspect.handler.catchAccessException(new AccessException("Ainda não Implementado"));
		else
			return proceed(source,access);
		return null;
	} 

}
