package arc.access;

import java.util.Arrays;

import arc.access.annotations.AccessPermission;

public abstract class AbstractAccessChecker implements AccessChecker{

	protected String user = "root";
	protected String[] groups = new String[] { "root" };

	@Override
	public boolean check(AccessPermission accesssPermission, AccessType accessType) {
		if (check(accesssPermission.otherAccess(), accessType))
			return true;
		if (this.user.equals(accesssPermission.owner()))
			if (check(accesssPermission.ownerAccess(), accessType))
				return true;
		if (Arrays.asList(groups).contains(accesssPermission.group()))
			if (check(accesssPermission.groupAccess(), accessType))
				return true;
		return false;
	}

	private boolean check(byte access, AccessType accessType) {
		switch (accessType) {
		case EXECUTE:
			return (access & (byte) 1) == (byte) 1;
		case WRITE:
			return (access & (byte) 2) == (byte) 2;
		case READ:
			return (access & (byte) 4) == (byte) 4;
		default:
			return false;
		}
	}

}
