package arc.access.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD,ElementType.METHOD,ElementType.TYPE,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessPermission {

	String owner() default "root";
	String group() default "root";
	
	byte ownerAccess() default 7;
	byte groupAccess() default 4;
	byte otherAccess() default 0;
	
	
}
