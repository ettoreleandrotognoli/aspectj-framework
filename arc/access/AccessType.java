package arc.access;

public enum AccessType {
	READ,
	WRITE,
	EXECUTE
}
