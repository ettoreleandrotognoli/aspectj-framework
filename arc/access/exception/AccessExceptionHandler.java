package arc.access.exception;


public interface AccessExceptionHandler {

	public void catchAccessException(AccessException exception);
	
}
