package arc.access.exception;

public class AccessException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public AccessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
