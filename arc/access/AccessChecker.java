package arc.access;

import arc.access.annotations.AccessPermission;

public interface AccessChecker {

	public boolean check(AccessPermission accesssPermission,AccessType accessType);
	
}
