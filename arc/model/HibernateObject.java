package arc.model;

public interface HibernateObject extends PersistenceObject{

	@Override
	public void save() throws Exception;
	
	@Override
	public void load() throws Exception;
	
	@Override
	public void delete() throws Exception;
	
}
