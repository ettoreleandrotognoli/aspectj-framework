package arc.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class DebugModelListener implements PropertyChangeListener,ActionListener {

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println(evt);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		System.out.println(evt);
	}


}
