package arc.model;

public interface PersistenceObject {

	public void save() throws Exception;
	
	public void load() throws Exception;
	
	public void delete() throws Exception;
	
}
