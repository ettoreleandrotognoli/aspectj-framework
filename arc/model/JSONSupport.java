package arc.model;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSONSupport {

	public String toJSON();
	public JSONObject toJSONObject();
	public void fromJSON(String json) throws JSONException;
}
