package arc.model;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

public interface Observable {
	
	public void addPropertyChangeListener(PropertyChangeListener listener);
	
	public void addPropertyChangeListener(String property,PropertyChangeListener listener);
	
	public void removePropertyChangeListener(PropertyChangeListener listener);
	
	public void removePropertyChangeListener(String property,PropertyChangeListener listener);
	
	public void addActionListener(ActionListener actionListener);
	
	public void removeActionListener(ActionListener actionListener);
	
}
