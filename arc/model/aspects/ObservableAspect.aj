package arc.model.aspects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;

import javax.swing.event.EventListenerList;

import arc.model.Observable;
import arc.model.annotations.Action;
import arc.model.annotations.Property;
import arc.util.Reflection;

public privileged aspect ObservableAspect {

	transient private Map<String,Object> Observable.backup = null;
	transient private Reflection<?> Observable.reflection = new Reflection<>(this);
	transient private PropertyChangeSupport Observable.propertyChangeSupport = new PropertyChangeSupport(this);
	transient private EventListenerList Observable.eventListenerList = new EventListenerList();
	
	private void Observable.firePropertyChange(String property,Object oldValue,Object newValue){
		this.propertyChangeSupport.firePropertyChange(property,oldValue,newValue);
	}

	public void Observable.addPropertyChangeListener(PropertyChangeListener listener) {
		this.propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	public void Observable.addPropertyChangeListener(String property,PropertyChangeListener listener) {
		this.propertyChangeSupport.addPropertyChangeListener(property,listener);
	}

	public void Observable.removePropertyChangeListener(PropertyChangeListener listener) {
		this.propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	public void Observable.removePropertyChangeListener(String property,PropertyChangeListener listener) {
		this.propertyChangeSupport.removePropertyChangeListener(property,listener);
	}
	
	public void Observable.addActionListener(ActionListener actionListener){
		this.eventListenerList.add(ActionListener.class,actionListener);
	}
	
	public void Observable.removeActionListener(ActionListener actionListener){
		this.eventListenerList.remove(ActionListener.class,actionListener);
	}
	
	private void Observable.fireActionPerformed(ActionEvent evt){
		for(ActionListener actionListener : this.eventListenerList.getListeners(ActionListener.class)){
			actionListener.actionPerformed(evt);
		}
	}
	
	private void Observable.prepareToChange(){
		this.backup = this.reflection.makeBackup();
	}
	
	private void Observable.happenedChange(){
		for(String p : reflection.getPropertys()){
			Object newVal = this.reflection.get(p);
			Object oldVal = this.backup.get(p);
			if(oldVal == null ? newVal != null : !oldVal.equals(newVal))
				this.propertyChangeSupport.firePropertyChange(p,oldVal,newVal);
		}
		this.backup = null;
	}
	
	pointcut propertyChange(Observable source, Object newVal, Property property)
		:set(@Property * *) && target(source) && args(newVal) && @annotation(property);

	Object around(Observable source, Object newVal, Property property):
		(propertyChange(source,newVal,property)){
		String pName = property.value().equals("")?thisJoinPoint.getSignature().getName():property.value();
		Object oldVal = source.reflection.get(pName);
		Object ret = proceed(source, newVal, property);
		if(oldVal == null ? newVal != null : !oldVal.equals(newVal))
			source.propertyChangeSupport.firePropertyChange(pName,oldVal,newVal);
		return ret;
	}
	
	pointcut actionEvent(Observable source,Action action)
		: execution(@Action * *.*(..)) && target(source) && @annotation(action);

	after(Observable source,Action action):(actionEvent(source,action)){
		String strAction = action.value();
		if(strAction.equals("")){
			String[] split = thisJoinPoint.getSignature().getName().split("\\.");
			strAction = split[split.length-1];
		}
		source.fireActionPerformed( new ActionEvent(this,0,strAction));
	}

	pointcut saveObservable(Observable source) : execution(* *.save(..)) && target(source);
	
	pointcut loadObservable(Observable source) : execution(* *.load(..)) && target(source);
	
	before (Observable source):(saveObservable(source) || loadObservable(source)){
		source.prepareToChange();
	}
	
	after (Observable source):(saveObservable(source) || loadObservable(source)){
		source.happenedChange();
	}
	
	
}
