package arc.model.aspects;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import arc.model.HibernateObject;
import arc.model.annotations.Action;

public aspect HibernateAspect {

	private static EntityManager entityManager = null;

	public static void init(String hibernate) throws Exception {
		HibernateAspect.entityManager = Persistence.createEntityManagerFactory(hibernate)
				.createEntityManager();
	}

	@Action
	public void HibernateObject.save() {
		HibernateAspect.entityManager.getTransaction().begin();
		try {
			HibernateAspect.entityManager.persist(this);
			HibernateAspect.entityManager.getTransaction().commit();
		}
		catch (Exception ex) {
			HibernateAspect.entityManager.getTransaction().rollback();
			throw ex;
		}
	}

	@Action
	public void HibernateObject.load() {
		HibernateAspect.entityManager.getTransaction().begin();
		try {
			HibernateAspect.entityManager.refresh(this);
			HibernateAspect.entityManager.getTransaction().commit();
		}
		catch (Exception ex) {
			HibernateAspect.entityManager.getTransaction().rollback();
			throw ex;
		}
	}

	@Action
	public void HibernateObject.delete() {
		HibernateAspect.entityManager.getTransaction().begin();
		try{
			HibernateAspect.entityManager.remove(this);
			HibernateAspect.entityManager.getTransaction().commit();	
		}
		catch(Exception ex){
			HibernateAspect.entityManager.getTransaction().rollback();
			throw ex;
		}
	}

}
