package arc.model.aspects;

import org.json.JSONException;
import org.json.JSONObject;

import arc.model.JSONSupport;
import arc.util.Cast;
import arc.util.Reflection;

public privileged aspect JSONAspect {

	public String JSONSupport.toJSON() {
		return this.toJSONObject().toString();
	}

	public JSONObject JSONSupport.toJSONObject() {
		JSONObject json = new JSONObject();
		Reflection<?> reflection = new Reflection<>(this);
		for (String p : reflection.getPropertys()) {
			try {
				Object o = reflection.get(p);
				if (JSONSupport.class.isInstance(o))
					json.put(p, ((JSONSupport) o).toJSONObject());
				else
					json.put(p, o);

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	public void JSONSupport.fromJSON(String json) throws JSONException {
		JSONObject jobj = new JSONObject(json);
		Reflection<?> reflection = new Reflection<>(this);
		for (String p : reflection.getPropertys()) {
			if(jobj.isNull(p)){
				continue;
			}
			Class<?> type = reflection.getPropertyType(p);
			Object value = jobj.get(p);
			if(JSONSupport.class.isAssignableFrom(type)){
				String jsObj = value.toString();
				try{
					value = type.newInstance();
					((JSONSupport)value).fromJSON(jsObj);	
				}
				catch(Exception ex){
					
				}
			}
			else if(type.isEnum()){
				value = Cast.enumCast(type, value);
			}
			else if(type != value.getClass()){
				value = Cast.cast(type, value);
			}
			reflection.set(p,value);
		}
	}
}
