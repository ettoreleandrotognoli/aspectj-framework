package arc.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JOptionPane;

import arc.util.ReflectionSupport;

public class GenericActionListener implements ActionListener{
	
	Method method;
	Object target;
	Object[] args;
	
	public GenericActionListener(Object obj, String action){
		this.target = obj;
		this.method = ReflectionSupport.getInstance(obj.getClass()).getAction(action);
		this.args = null;
	}

	public GenericActionListener(Method method, Object target, Object[] args) {
		this.method = method;
		this.target = target;
		this.args = args;
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		try {
			this.method.invoke(this.target,this.args);
		}
		catch (IllegalAccessException|IllegalArgumentException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			System.out.println("Aqui mesmo!!!");
		}
		catch(InvocationTargetException ex){
			JOptionPane.showMessageDialog(null,ex.getTargetException().getMessage(),
											 ex.getTargetException().getClass().toString(),
											 JOptionPane.ERROR_MESSAGE);
		}
	}

}
