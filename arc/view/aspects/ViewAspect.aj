package arc.view.aspects;

import java.lang.reflect.Field;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

import arc.view.ClassResourceBundle;
import arc.view.View;
import arc.view.annotatios.BindProperty;

public aspect ViewAspect {

	transient private BindingGroup View.bindingGroup = null;
	transient private E View<E>.model = null;

	public E View<E>.getModel(){
		return this.model;
	}
	
	public void View<E>.setModel(E model){
		if(this.bindingGroup != null)
			this.destroyBind();
		this.model = model;
		this.createBind();
	}
	
	private void View.createBind(){
		this.bindingGroup = new BindingGroup();
		this.bindPropertys();
		this.bindingGroup.bind();
	}
	
	private void View.destroyBind(){
		this.bindingGroup.unbind();
		this.bindingGroup = null;
	}
	
	private void View.bindPropertys(){
		Field fields[] = this.getClass().getDeclaredFields();
		for(Field field : fields){
			BindProperty bindProperty = field.getAnnotation(BindProperty.class);
			if(bindProperty==null)
				continue;
			ELProperty<Object,Object> elProperty = ELProperty.create("${model."+bindProperty.modelProperty()+"}");
			BeanProperty<Object,Object> beanProperty = BeanProperty.create(bindProperty.componentProperty());
			UpdateStrategy updateStrategy = bindProperty.updateStrategy();
			Object component;
			try {
				field.setAccessible(true);
				component = field.get(this);
				Binding<Object,Object,Object,Object> bind = Bindings.createAutoBinding(updateStrategy, this, elProperty,component,beanProperty);
				this.bindingGroup.addBinding(bind);
			}
			catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
