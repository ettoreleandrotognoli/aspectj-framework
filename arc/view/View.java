package arc.view;

import arc.model.Observable;

public interface View<E extends Observable> {
	
	public final static String TEXT = "text";
	public final static String TEXT_ON_ACTION_OR_FOCUS_LOST = "text_ON_ACTION_OR_FOCUS_LOST";
	public final static String SELECTED = "selected";
	public final static String SELECTED_ITEM = "selectedItem";
	public final static String VALUE = "value";

	public E getModel();
	
	public void setModel(E model);
	
}
