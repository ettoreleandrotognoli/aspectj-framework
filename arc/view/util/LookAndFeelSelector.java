package arc.view.util;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

public class LookAndFeelSelector extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JComboBox<UIManager.LookAndFeelInfo> cbLook = new JComboBox<>(UIManager.getInstalledLookAndFeels());
	
	UIManager.LookAndFeelInfo[] lafs;

	public LookAndFeelSelector() {
		super();
		init();
	}

	public LookAndFeelSelector(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		init();
	}
	
	private void init(){
		this.setLayout(new BorderLayout());
		JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		northPanel.add(this.cbLook);
		this.add(northPanel,BorderLayout.NORTH);
		this.initEvents();
	}
	
	private void initEvents(){
		this.cbLook.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				UIManager.LookAndFeelInfo lookInfo =
						(LookAndFeelInfo) LookAndFeelSelector.this.cbLook.getSelectedItem();
				LookAndFeelSelector.this.setLookAndFeel(lookInfo);
			}
		});
	}
	
	public static void main(String... args){
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout(FlowLayout.CENTER));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(640,480);
		frame.setLocationRelativeTo(null);
		frame.add(new LookAndFeelSelector());
		frame.setVisible(true);
	}
	
	private void setLookAndFeel(UIManager.LookAndFeelInfo lookInfo){
		try {
			UIManager.setLookAndFeel(lookInfo.getClassName());
			for(Frame frame : Frame.getFrames()){
				updateLAFRecursively(frame);
			}
		}
		catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(this
					,"Mensagem"
					,"Titulo"
					,JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void updateLAFRecursively(Window window) {
	    for (Window childWindow : window.getOwnedWindows()) {
	        updateLAFRecursively(childWindow);
	    }
	    SwingUtilities.updateComponentTreeUI(window);
	}
	
	

}
