package arc.view.adaptable;

import javax.swing.JComponent;

public interface ComponentFactory {

	public JComponent makeComponent(Class<?> type);
	
}
