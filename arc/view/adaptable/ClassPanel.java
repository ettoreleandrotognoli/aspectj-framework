package arc.view.adaptable;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

import arc.JSONTest;
import arc.Model;
import arc.view.View;
import arc.view.dialog.Dialog;

public class ClassPanel extends JPanel {

	private BindingGroup bindingGroup;
	private JLabel lbCombo = new JLabel();
	private JComboBox cbClass = new JComboBox<>(new DefaultComboBoxModel<>());
	private FieldPanel fieldPanel = new FieldPanel();
	

	public ClassPanel() {
		super();
		this.initComponents();
	}

	public ClassPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		this.initComponents();
	}

	public ClassPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		this.initComponents();
	}

	public ClassPanel(LayoutManager layout) {
		super(layout);
		this.initComponents();
	}
	
	public void addOption(Object... values){
		DefaultComboBoxModel<Object> model = (DefaultComboBoxModel<Object>) this.cbClass.getModel();
		for(Object value : values)
			model.addElement(value);
	}

	private void initComponents() {
		this.setLayout(new BorderLayout());
		JPanel north = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		lbCombo.setText("->");
		lbCombo.setLabelFor(cbClass);
		north.add(lbCombo);
		north.add(cbClass);
		bindingGroup = new BindingGroup();
		ELProperty elProperty = ELProperty.create("${model}");
		BeanProperty beanProperty = BeanProperty.create(View.SELECTED_ITEM);
		UpdateStrategy updateStrategy = UpdateStrategy.READ_WRITE;
		Binding bind = Bindings.createAutoBinding(updateStrategy,fieldPanel,elProperty,cbClass,beanProperty);
		bindingGroup.addBinding(bind);
		bindingGroup.bind();
		this.add(north,BorderLayout.NORTH);
		this.add(new JScrollPane(fieldPanel),BorderLayout.CENTER);
	}
	
	public FieldPanel getFieldPanel() {
		return fieldPanel;
	}

	public static void main(String... args){
		ClassPanel classPanel = new ClassPanel();
		classPanel.addOption(new JSONTest(),new Model());
		Dialog.showDialog(classPanel);
	}
	
}
