package arc.view.adaptable;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.swingx.VerticalLayout;

import arc.access.AccessType;
import arc.access.annotations.AccessPermission;
import arc.model.Observable;
import arc.util.ReflectionSupport;
import arc.view.ClassResourceBundle;
import arc.view.View;
import arc.access.aspects.AccessAspect;

public class FieldPanel extends JPanel{
	
	public static final String PROP_MODEL = "model";
	public static final String PROP_META_MODEL = "metaModel";
	
	private static final long serialVersionUID = 1L;
	
	transient private BindingGroup bindingGroup;
	transient private ComponentFactory componentFactory = new DefaultComponentFactory();
	transient private Class<?> metaModel;
	private Observable model;
	private ClassResourceBundle bundle;
	private Map<String,JComponent> components = new HashMap<>();

	public FieldPanel() {
		super();
	}

	public FieldPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
	}

	public FieldPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}

	public FieldPanel(LayoutManager layout) {
		super(layout);
	}

	public void setModel(Observable model){
		if(this.model == model)
			return;
		if(model != null && this.metaModel != model.getClass()){
			this.makeComponents(model.getClass());
			this.makeView();
		}
		if(this.bindingGroup != null)
			this.unbind();
		Object oldValue = this.model;
		this.model = model;
		this.firePropertyChange(PROP_MODEL,oldValue,this.model);
		this.bind();
	}
	
	public Class<?> getMetaModel() {
		return metaModel;
	}

	public void setMetaModel(Class<?> metaModel) {
		if(this.metaModel != metaModel){
			Class<?> oldValue = this.metaModel;
			this.metaModel = metaModel;
			this.makeComponents(metaModel);
			this.makeView();
			this.firePropertyChange(PROP_META_MODEL, oldValue, metaModel);
		}
	}

	public Observable getModel(){
		return this.model;
	}
	
	private void unbind(){
		this.bindingGroup.unbind();
		this.bindingGroup = null;
	}
	
	private void bind(){
		this.bindingGroup = new BindingGroup();
		this.bindComponents();
		this.bindingGroup.bind();
	}
	
	private void makeComponents(Class<?> metaModel){
		this.metaModel = metaModel;
		this.bundle = new ClassResourceBundle(metaModel);
		this.components.clear();
		ReflectionSupport<?> reflectionSupport = ReflectionSupport.getInstance(metaModel);
		for(String property : reflectionSupport.getPropertys()){
			JComponent component = this.componentFactory.makeComponent(reflectionSupport.getPropertyType(property));
			component.setName(property);
			component.setToolTipText(bundle.getToolTip(property));
			AccessPermission access = reflectionSupport.getPropertyAccessPermission(property);
			if(!AccessAspect.check(access,AccessType.WRITE))
				component.setEnabled(false);
			if(AccessAspect.check(access,AccessType.READ))
				this.components.put(property,component);	
		}
	}
	
	private void bindComponents(){
		for(JComponent component : this.components.values()){
			String property = component.getName();
			ELProperty<Object,Object> elProperty = ELProperty.create("${model."+property+"}");
			BeanProperty<Object,Object> beanProperty = BeanProperty.create(componentProperty(component));
			UpdateStrategy updateStrategy = UpdateStrategy.READ_WRITE;
			Binding<Object, Object, Object, Object> bind = Bindings.createAutoBinding(updateStrategy,this,elProperty,component,beanProperty);
			this.bindingGroup.addBinding(bind);
		}
	}
	
	private String componentProperty(JComponent component){
		if(component instanceof JCheckBox)
			return View.SELECTED;
		if(component instanceof JComboBox)
			return View.SELECTED_ITEM;
		if(component instanceof FieldPanel)
			return PROP_MODEL;	
		if(component instanceof JFormattedTextField)
			return View.VALUE;
		return View.TEXT_ON_ACTION_OR_FOCUS_LOST;
	}
	
	private void makeView(){
		this.removeAll();
		this.setToolTipText(this.bundle.getToolTip(this.metaModel.getSimpleName()));
		this.setBorder(BorderFactory.createTitledBorder(this.bundle.getString(this.metaModel.getSimpleName())));
		this.setLayout(new VerticalLayout());
		for(JComponent component : this.components.values()){
			JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			JLabel label = new JLabel();
			label.setText(this.bundle.getString(component.getName()));
			label.setLabelFor(component);
			panel.add(label);
			panel.add(component);
			this.add(panel);
		}
		if(this.getRootPane()!=null){
			this.getRootPane().validate();
		}
		/*for(Container obj = this.getRootPane(); obj != null ; obj = obj.getParent())
		if(obj instanceof Window)
			((Window)obj).validate();*/
		//this.getRootPane().repaint();
	}
	
	public void setEnabled(boolean enabled){
		ReflectionSupport<?> reflectionSupport = ReflectionSupport.getInstance(this.metaModel);
		for(JComponent component : this.components.values()){
			AccessPermission access = reflectionSupport.getPropertyAccessPermission(component.getName());
			boolean hasAccess = AccessAspect.check(access,AccessType.WRITE);
			component.setEnabled(enabled & hasAccess);
		}
	}
	
}
