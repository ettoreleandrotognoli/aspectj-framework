package arc.view.adaptable;

import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Date;

import javassist.bytecode.FieldInfo;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

import arc.model.Observable;

public class DefaultComponentFactory implements ComponentFactory {
	
	int width = 100;
	int height = 20;

	@Override
	public JComponent makeComponent(Class<?> type) {
		JComponent component = new JTextField();
		((JTextField)component).setHorizontalAlignment(JTextField.CENTER);
		if(type.isEnum()){
			component =  new JComboBox<>(type.getEnumConstants());
		}
		if(type == boolean.class){
			component = new JCheckBox();
		}
		if(type == Date.class ){
			component = new JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
		}
		if(Observable.class.isAssignableFrom(type)){
			FieldPanel panel =  new FieldPanel();
			panel.setMetaModel(type);
			return panel;
		}
		component.setSize(width,height);
		component.setPreferredSize(new Dimension(width,height));
		return component;
	}

}
