package arc.view;

import java.util.ResourceBundle;

public class ClassResourceBundle{
	
	private static ResourceBundle generalBundle;
	
	static{
		try{
			ClassResourceBundle.generalBundle = ResourceBundle.getBundle("");
		}
		catch(Exception ex){
			
		}
	}

	ResourceBundle bundle = null;
	
	public ClassResourceBundle(Class<?> clazz){
		try{
			bundle = ResourceBundle.getBundle(clazz.getName());
		}
		catch(Exception ex){
			
		}
	}
	
	
	public String getString(String key){
		if(this.bundle != null && this.bundle.containsKey(key)){
			return bundle.getString(key);
		}
		else if(ClassResourceBundle.generalBundle != null && ClassResourceBundle.generalBundle.containsKey(key)){
			return ClassResourceBundle.generalBundle.getString(key);
		}
		else{
			return key;
		}
	}
	
	public String getToolTip(String key){
		return this.getString("_"+key);
	}
	
	
}
