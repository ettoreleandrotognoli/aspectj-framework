package arc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.jdesktop.swingx.VerticalLayout;

public class GenericPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	JPanel fieldPanel = new JPanel(new VerticalLayout());
	JPanel actionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

	public GenericPanel() {
		super();
		this.init();
	}

	public GenericPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		this.init();
	}

	public GenericPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		this.init();
	}

	public GenericPanel(LayoutManager layout) {
		super(layout);
		this.init();
	}
	
	public void init(){
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(this.fieldPanel),BorderLayout.CENTER);
		//this.add(this.fieldPanel,BorderLayout.CENTER);
		this.add(this.actionPanel,BorderLayout.SOUTH);
	}
	
	public void addFieldComponent(String label,JComponent[] components){
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel pn = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pn.setBorder(BorderFactory.createTitledBorder(panel.getBorder(),label));
		for(JComponent c : components){
			pn.add(c);
		}
		panel.add(pn);
		this.fieldPanel.add(panel);
	}
	
	public void addFieldComponent(String label,JComponent component){
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel lb = new JLabel(label);
		lb.setLabelFor(component);
		panel.add(lb);
		panel.add(component);
		this.fieldPanel.add(panel);
	}
	
	public void addFieldComponent(JComponent... components){
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		for(JComponent c : components){
			panel.add(c);
		}
		this.fieldPanel.add(panel);
	}
	
	public void clearFields(){
		this.fieldPanel.removeAll();
	}
	
	public void addActionComponent(JComponent...components){
		for(JComponent c : components){
			this.actionPanel.add(c);
		}
	}
	
	public void clearActions(){
		this.actionPanel.removeAll();
	}
	
	@Override
	public void removeAll(){
		this.actionPanel.removeAll();
		this.actionPanel.removeAll();
	}
	
	
	public static void main(String args[]){
		GenericPanel panel = new GenericPanel();
		JFrame frame = new JFrame();
		frame.setSize(new Dimension(640,480));
		frame.setLayout(new BorderLayout());
		panel.addFieldComponent(new JComboBox<String>(new String[]{"teste","otro teste","mais um teste"}));
		panel.addFieldComponent(new JLabel("humm"));
		panel.addActionComponent(new JButton("fuu"));
		panel.addFieldComponent("Nome:",new JTextField(20));
		JPanel pn = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pn.setBorder(BorderFactory.createTitledBorder(pn.getBorder(),"Label de Borda"));
		pn.add(new JTextField(30));
		panel.addFieldComponent(pn);
		frame.add(panel,BorderLayout.CENTER);
		frame.setVisible(true);
	}

}
