package arc.view.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import arc.model.Observable;
import arc.view.ClassResourceBundle;
import arc.view.adaptable.FieldPanel;

public class Dialog extends JDialog{
	
	private static final long serialVersionUID = 1L;

	public enum DialogResponse{
		Ok,
		Cancel,
		Closed
	}
	
	private ClassResourceBundle bundle = new ClassResourceBundle(Dialog.class);
	private DialogResponse response = DialogResponse.Closed;
	private JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
	
	public static <E extends Observable> E editDialog(E toEdit){
		Dialog dialog = new Dialog();
		dialog.setModal(true);
		FieldPanel fieldPanel = new FieldPanel();
		fieldPanel.setModel(toEdit);
		dialog.add(new JScrollPane(fieldPanel),BorderLayout.CENTER);
		dialog.addDefaultOptions();
		dialog.setVisible(true);
		if(dialog.getResponse() != DialogResponse.Ok){
			return null;
		}
		return toEdit;
	}
	
	public static <E extends Observable> E createDialog(Class<E> classType){
		Dialog dialog = new Dialog();
		dialog.setModal(true);
		FieldPanel fieldPanel = new FieldPanel();
		E toEdit;
		try {
			toEdit = classType.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
		fieldPanel.setModel(toEdit);
		dialog.add(new JScrollPane(fieldPanel),BorderLayout.CENTER);
		dialog.addDefaultOptions();
		dialog.setVisible(true);
		if(dialog.getResponse() != DialogResponse.Ok){
			return null;
		}
		return toEdit;
	}
	
	public static void showDialog(Observable model){
		Dialog dialog = new Dialog();
		dialog.setModal(true);
		FieldPanel fieldPanel = new FieldPanel();
		fieldPanel.setModel(model);
		dialog.add(new JScrollPane(fieldPanel),BorderLayout.CENTER);
		dialog.addDefaultOptions();
		fieldPanel.setEnabled(false);
		dialog.setVisible(true);
	}
	
	public static void showDialog(JComponent component){
		Dialog dialog = new Dialog();
		dialog.setModal(true);
		dialog.add(component,BorderLayout.CENTER);
		dialog.setVisible(true);
	}
	
	
	public static <E extends Observable> E selectDialog(List<E> list){
		return null;
	}
	
	protected Dialog(){
		super();
		this.init();
	}
	
	private void init(){
		KeyStroke escStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0);
		ActionListener actionClose = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Dialog.this.dispose();
			}
		};
		this.getRootPane().registerKeyboardAction(actionClose, escStroke,JComponent.WHEN_IN_FOCUSED_WINDOW);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLayout(new BorderLayout());
		this.add(this.southPanel,BorderLayout.SOUTH);
	}
	
	protected void addDefaultOptions(){
		this.addOption("cancel",new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Dialog.this.response = DialogResponse.Cancel;
				Dialog.this.dispose();
			}
		});
		this.addOption("confirm",new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Dialog.this.response = DialogResponse.Ok;
				Dialog.this.dispose();
			}
		});
	}
	
	protected void addOption(String key,ActionListener... actionListener){
		this.addOption(bundle.getString(key),bundle.getToolTip(key), actionListener);
	}
	
	protected void addOption(String text,String toolTip, ActionListener... actionListener){
		JButton button = new JButton(text);
		button.setToolTipText(toolTip);
		for(ActionListener action : actionListener){
			button.addActionListener(action);	
		}
		this.southPanel.add(button);
	}
	
	public DialogResponse getResponse(){
		return this.response;
	}
	
	public void setResponde(DialogResponse response){
		this.response = response;
	}
	
	@Override
	public void setVisible(boolean visible){
		if(!visible){
			super.setVisible(false);
			return;
		}
		this.pack();
		this.setLocationRelativeTo(null);
		super.setVisible(true);
	}
	
}
