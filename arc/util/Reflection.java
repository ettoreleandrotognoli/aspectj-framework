package arc.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Reflection<E>{

	ReflectionSupport<E> support;
	E target;
	
	public Reflection(Class<E> clazz) throws InstantiationException, IllegalAccessException{
		this.support = ReflectionSupport.getInstance(clazz);
		this.target = clazz.newInstance();
	}
	
	@SuppressWarnings("unchecked")
	public Reflection(E target){
		this.support = (ReflectionSupport<E>) ReflectionSupport.getInstance(target.getClass());
		this.target = target;
	}
	
	public String[] getPropertys(){
		return this.support.getPropertys();
	}
	
	public Class<?> getPropertyType(String property){
		return this.support.getPropertyType(property);
	}
	
	public void set(String property, Object value){
		try {
			this.support.getPropertyField(property).set(this.target,value);
		}
		catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Object get(String property){
		try {
			return this.support.getPropertyField(property).get(this.target);
		}
		catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void setProperty(String property, Object value){
		Method m = this.support.getPropertySet(property);
		if(m == null){
			this.set(property, value);
			return;
		}
		try {
			m.invoke(this.target,value);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public Object getProperty(String property){
		Method m = this.support.getPropertyGet(property);
		if(m == null){
			return this.get(property);
		}
		try {
			return m.invoke(this.target);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public E getTarget(){
		return this.target;
	}
	
	public Class<E> getTargetType(){
		return this.support.getTarget();
	}
	
	public Map<String,Object> makeBackup(){
		Map<String,Object> backup = new HashMap<>();
		for(String p : this.support.getPropertys()){
			backup.put(p,this.get(p));
		}
		return backup;
	}
	
	public void restoreBackup(Map<String,Object> backup){
		for(String p : this.support.getPropertys()){
			this.setProperty(p,backup.get(p));
		}
	}
	
	public void setFields(E other){
		Reflection<E> reflection = new Reflection<>(other);
		for(String p : support.getPropertys()){
			this.setProperty(p,reflection.get(p));
		}
	}
	
	public Object executeAction(String action,Object... args){
		Method m = this.support.getAction(action);
		if(m == null){
			return null;
		}
		try {
			return m.invoke(this.target, args);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}
}
