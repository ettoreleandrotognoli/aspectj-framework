package arc.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import arc.access.annotations.AccessPermission;
import arc.model.annotations.Action;
import arc.model.annotations.Property;

public class ReflectionSupport<E> {

	private static Map<Class<?>, ReflectionSupport<?>> instances = new HashMap<>();

	private class FieldMethodNode {
		public Field field;
		public Method getMethod;
		public Method setMethod;
	}

	private Class<E> target;
	private Map<String, FieldMethodNode> propertyAccess = new HashMap<>();
	private Map<String, Method> actionAccess = new HashMap<>();

	private ReflectionSupport(Class<E> clazz) {
		this.target = clazz;
		this.initPropertyFields();
		this.initActionMethods();
	}

	private void initActionMethods() {
		Class<?> clazz = this.target;
		for (Method m : clazz.getMethods()) {
			Action a = m.getAnnotation(Action.class);
			if (a == null) {
				continue;
			}
			String aName = a.value().equals("") ? m.getName() : a.value();
			actionAccess.put(aName, m);
		}
	}

	private void initPropertyFields() {
		Class<?> clazz = this.target;
		while (clazz != null) {
			for (Field f : clazz.getDeclaredFields()) {
				Property p = f.getAnnotation(Property.class);
				if (p == null) {
					continue;
				}
				String pName = p.value().equals("") ? f.getName() : p.value();
				makeNode(clazz, pName, f);
			}
			clazz = clazz.getSuperclass();
		}
	}

	private void makeNode(Class<?> clazz, String property, Field f) {
		f.setAccessible(true);
		FieldMethodNode node = new FieldMethodNode();
		node.field = f;
		try {
			node.getMethod = getGetMethod(clazz, f);
		}
		catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			node.setMethod = getSetMethod(clazz, f);
		}
		catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.propertyAccess.put(property, node);
	}

	private Method getGetMethod(Class<?> clazz, Field field) throws NoSuchMethodException,
			SecurityException {
		return clazz.getMethod(makeGetMethodName(field));
	}

	private Method getSetMethod(Class<?> clazz, Field field) throws NoSuchMethodException,
			SecurityException {
		return clazz.getMethod(makeSetMethodName(field), field.getType());
	}

	private String makeGetMethodName(Field field) {
		String name = field.getName();
		name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
		if (field.getType() == boolean.class) {
			name = "is" + name;
		}
		else {
			name = "get" + name;
		}
		return name;
	}

	private String makeSetMethodName(Field field) {
		String name = field.getName();
		name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
		return "set" + name;
	}

	@SuppressWarnings("unchecked")
	public static <A> ReflectionSupport<A> getInstance(Class<A> clazz) {
		ReflectionSupport<A> rs = (ReflectionSupport<A>) ReflectionSupport.instances.get(clazz);
		if (rs == null) {
			rs = new ReflectionSupport<>(clazz);
			ReflectionSupport.instances.put(clazz, rs);
		}
		return rs;
	}

	public String[] getPropertys() {
		return this.propertyAccess.keySet().toArray(new String[this.propertyAccess.size()]);
	}

	public Field getPropertyField(String property) {
		return this.propertyAccess.get(property).field;
	}

	public Class<?> getPropertyType(String property) {
		return this.propertyAccess.get(property).field.getType();
	}

	public Method getPropertySet(String property) {
		return this.propertyAccess.get(property).setMethod;
	}

	public Method getPropertyGet(String property) {
		return this.propertyAccess.get(property).getMethod;
	}

	public Class<E> getTarget() {
		return this.target;
	}

	public String[] getActions() {
		return this.actionAccess.keySet().toArray(new String[this.actionAccess.size()]);
	}

	public Method getAction(String action) {
		return this.actionAccess.get(action);
	}
	
	public AccessPermission getActionAccessPermission(String action) {
		Method method = this.actionAccess.get(action);
		AccessPermission accessPermission = method.getAnnotation(AccessPermission.class);
		if (accessPermission == null)
			accessPermission = this.target.getAnnotation(AccessPermission.class);
		return accessPermission;
	}

	public AccessPermission getPropertyAccessPermission(String property) {
		Field field = this.propertyAccess.get(property).field;
		AccessPermission accessPermission = field.getAnnotation(AccessPermission.class);
		if (accessPermission == null)
			accessPermission = this.target.getAnnotation(AccessPermission.class);
		return accessPermission;
	}

}
