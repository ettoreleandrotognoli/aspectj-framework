package arc.util.holder;

import java.util.EventListener;

public interface HolderListener<E> extends EventListener {

	public void valueChanged(HolderEvent<E> event);
	
}
