package arc.util.holder;

import java.util.EventObject;

public class HolderEvent<E> extends EventObject {

	private static final long serialVersionUID = 1L;

	public E oldValue;
	public E newValue;

	@SuppressWarnings("unchecked")
	@Override
	public Holder<E> getSource() {
		return (Holder<E>) super.getSource();
	}

	public HolderEvent(Holder<E> source, E oldValue, E newValue) {
		super(source);
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public E getOldValue() {
		return oldValue;
	}

	public void setOldValue(E oldValue) {
		this.oldValue = oldValue;
	}

	public E getNewValue() {
		return newValue;
	}

	public void setNewValue(E newValue) {
		this.newValue = newValue;
	}

}
