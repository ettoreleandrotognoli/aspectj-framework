package arc.util.holder;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.event.EventListenerList;

import arc.model.Observable;
import arc.util.Reflection;

public class PropertyHolder<E> implements Holder<E>, PropertyChangeListener {

	private EventListenerList listenerList = new EventListenerList();
	private Reflection<? extends Observable> reflection;
	private String property;

	public PropertyHolder(Reflection<? extends Observable> reflection, String property) {
		this.reflection = reflection;
		this.property = property;
		this.reflection.getTarget().addPropertyChangeListener(property, this);
	}

	public PropertyHolder(Observable model, String property) {
		this.reflection = new Reflection<>(model);
		this.property = property;
		model.addPropertyChangeListener(property, this);
	}

	@Override
	public void setTarget(E target) {
		this.reflection.setProperty(property, target);
	}

	@SuppressWarnings("unchecked")
	@Override
	public E getTarget() {
		return ((E) this.reflection.get(property));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (!evt.getPropertyName().equals(this.property)) {
			return;
		}
		for (HolderListener<E> listener : this.listenerList.getListeners(HolderListener.class)) {
			listener.valueChanged(
					new HolderEvent<E>(this, (E) evt.getOldValue(), (E) evt.getNewValue())
			);
		}
	}

	@Override
	public void addHolderListener(HolderListener<E> hoderListener) {
		this.listenerList.add(HolderListener.class, hoderListener);
	}

	@Override
	public void removeHolderListener(HolderListener<E> hoderListener) {
		this.listenerList.remove(HolderListener.class, hoderListener);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<E> getType() {
		return (Class<E>) reflection.getPropertyType(property);
	}

}
