package arc.util.holder;


public interface Holder<E>{

	public Class<E> getType();
	
	public void setTarget(E target);
	
	public E getTarget();
	
	public void addHolderListener(HolderListener<E> hoderListener);
	
	public void removeHolderListener(HolderListener<E> hoderListener);
	
}
