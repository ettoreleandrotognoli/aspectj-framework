package arc.util;

public class Cast {

	public static Object enumCast(Class<?> enumType, Object value){
		Object[] elements = enumType.getEnumConstants();
		if(value instanceof Number){
			return elements[((Number)value).intValue()];
		}
		if(value instanceof String){
			for(Object obj : elements)
				if(obj.toString().equals(value))
					return obj;
		}
		return null;
	}
	
	public static Object cast(Class<?> type, Object value){
		if(Number.class.isInstance(value) && Number.class.isAssignableFrom(type)){
			return numberCast((Class<? extends Number>)type,(Number)value);
		}
		else if(type.isPrimitive() && !value.getClass().isPrimitive()){
			return value;
		}
		else{
			return type.cast(value);
		}
	}
	
	public static Object numberCast(Class<? extends Number> type, Number number){
		switch(type.getSimpleName()){
			case "Byte": return number.byteValue();
			case "Integer": return number.intValue();
			case "Long": return number.longValue();
			case "Float": return number.floatValue();
			case "Double": return number.doubleValue();
		}
		return number;
	}

}
